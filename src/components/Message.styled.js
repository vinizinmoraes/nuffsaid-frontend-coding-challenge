import styled from "styled-components";

const cardPriorityConfig = {
  0: {
    gridColumnStart: 1,
    color: "#F56236",
  },
  1: {
    gridColumnStart: 2,
    color: "#FCE788",
  },
  2: {
    gridColumnStart: 3,
    color: "#88FCA3",
  },
};

export const Card = styled.div`
  grid-column-start: ${(props) =>
    cardPriorityConfig[props.priority].gridColumnStart};
  background-color: ${(props) => cardPriorityConfig[props.priority].color};
  box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
  padding: 12px;
  min-height: 100px;
  display: inline-grid;
  grid-template-columns: auto;
  grid-template-rows: 1fr auto;
`;

export const Actions = styled.div`
  text-align: right;
`;

export const Button = styled.button`
  background: none;
  border: 0;
  cursor: pointer;
`;
