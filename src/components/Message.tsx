import { Message as MessageType, Priority } from "../Api";
import * as S from "./Message.styled";
import { SnackBarContext } from "../context/SnackBarContext";
import { useContext, useEffect } from "react";

type MessageProps = MessageType & {
  clearMessage: (message: string) => void;
};

export default function Message({
  message,
  priority,
  clearMessage,
}: MessageProps) {
  const { setSnackBar } = useContext(SnackBarContext);

  useEffect(() => {
    if (priority === Priority.Error) {
      setSnackBar({ message: message, open: true });
    }
  }, [priority, message, setSnackBar]);

  return (
    <S.Card priority={priority}>
      <p>{message}</p>
      <S.Actions>
        <S.Button
          onClick={() => {
            clearMessage(message);
          }}
        >
          Clear
        </S.Button>
      </S.Actions>
    </S.Card>
  );
}
