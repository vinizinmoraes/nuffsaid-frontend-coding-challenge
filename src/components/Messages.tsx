import { useState, useEffect } from "react";

import { Message as MessageType, Priority } from "../Api";
import Message from "./Message";
import * as S from "./Messages.styled";

type MessagesProps = {
  messages: MessageType[];
  clearMessage: (message: string) => void;
};

type CountType = {
  [Priority.Error]: number;
  [Priority.Info]: number;
  [Priority.Warn]: number;
};

type TitleProps = {
  text: string;
  priority: Priority;
  count: number;
};

const Title = ({ text, priority, count }: TitleProps) => {
  return (
    <S.Title priority={priority}>
      <h2>{text}</h2>
      <p>Count {count}</p>
    </S.Title>
  );
};

export default function Messages({ messages, clearMessage }: MessagesProps) {
  const [count, setCount] = useState({ 0: 0, 1: 0, 2: 0 });

  useEffect(() => {
    setCount(
      messages.reduce<CountType>(
        (acc, { priority }) => {
          acc[priority] += 1;
          return acc;
        },
        { 0: 0, 1: 0, 2: 0 }
      )
    );
  }, [messages]);

  return (
    <S.Main>
      <Title
        text="Error Type 1"
        count={count[Priority.Error]}
        priority={Priority.Error}
      />
      <Title
        text="Warning Type 2"
        count={count[Priority.Warn]}
        priority={Priority.Warn}
      />
      <Title
        text="Info Type 3"
        count={count[Priority.Info]}
        priority={Priority.Info}
      />
      {messages.map((message) => {
        return (
          <Message
            {...message}
            key={message.message}
            clearMessage={clearMessage}
          />
        );
      })}
    </S.Main>
  );
}
