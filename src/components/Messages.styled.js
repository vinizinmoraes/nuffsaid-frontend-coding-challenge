import styled from "styled-components";

export const Main = styled.main`
  display: grid;
  grid-template-areas: "area1 area2 area3";
  grid-template-columns: 1fr 1fr 1fr;
  grid-auto-flow: dense;
  grid-gap: 12px;
  max-width: 1024px;
  margin: 0 auto;
`;

export const Title = styled.div`
  grid-column-start: ${(props) => props.priority + 1};
`;
