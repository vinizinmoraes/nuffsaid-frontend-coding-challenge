import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import App from "../App";

test("Render the page", () => {
  render(<App />);
  const h1 = screen.getByText(/nuffsaid.com Coding Challenge/i);
  expect(h1).toBeInTheDocument();
});

test("wait for a card to open on the page and clear it", async () => {
  render(<App />);
  const clearButton = screen.getAllByText(/clear/i)[1];
  expect(clearButton).toBeInTheDocument();
  userEvent.click(clearButton);
  expect(clearButton).not.toBeInTheDocument();
});
