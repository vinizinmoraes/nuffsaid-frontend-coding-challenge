import styled from "styled-components";

export const Actions = styled.div`
  display: grid;
  grid-template-columns: min-content min-content;
  justify-content: center;
  margin-bottom: 30px;
  grid-gap: 10px;
  & button {
    background-color: #88fca3;
    box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
    border: 0;
    text-transform: uppercase;
    font-weight: bold;
    padding: 8px 14px;
    cursor: pointer;
  }
`;

export const PageTitle = styled.h1`
  border-bottom: solid 4px #ccc;
  padding: 0.67rem 0;
  margin: 0 0 10px 0;
`;
