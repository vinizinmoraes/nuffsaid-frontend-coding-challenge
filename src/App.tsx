import React, { useState } from "react";
import { useEffect } from "react";
import generateMessage, { Message } from "./Api";
import Messages from "./components/Messages";
import { SnackBarProvider } from "./context/SnackBarContext";
import * as S from "./App.styled";

const App: React.FC<{}> = () => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [pullMessages, setPullMessages] = useState(true);

  useEffect(() => {
    if (pullMessages) {
      const cleanUp = generateMessage((message: Message) => {
        setMessages((oldMessages) => [message, ...oldMessages]);
      });
      return cleanUp;
    }
  }, [setMessages, pullMessages]);

  function clearMessage(messageToBeRemoved: string) {
    setMessages(
      messages.filter(({ message }) => message !== messageToBeRemoved)
    );
  }

  return (
    <div>
      <S.PageTitle>nuffsaid.com Coding Challenge</S.PageTitle>
      <S.Actions>
        <button
          onClick={() => {
            setPullMessages(!pullMessages);
          }}
        >
          {pullMessages ? "Stop" : "Start"}
        </button>
        <button
          onClick={() => {
            setMessages([]);
          }}
        >
          Clear
        </button>
      </S.Actions>
      <SnackBarProvider>
        <Messages messages={messages} clearMessage={clearMessage} />
      </SnackBarProvider>
    </div>
  );
};

export default App;
