import Snackbar from "@mui/material/Snackbar";
import React, { createContext, ReactNode, useState } from "react";

export type SnackBarContextType = {
  snackBar: { message: string; open: boolean };
  setSnackBar: (message: { message: string; open: boolean }) => void;
};

export const SnackBarContext = createContext<SnackBarContextType>({
  snackBar: { message: "", open: false },
  setSnackBar: () => {},
});

export const SnackBarProvider = ({ children }: { children: ReactNode }) => {
  const [snackBar, setSnackBar] = useState({ message: "", open: false });

  return (
    <SnackBarContext.Provider value={{ snackBar, setSnackBar }}>
      <Snackbar
        open={snackBar.open}
        autoHideDuration={2000}
        message={snackBar.message}
        onClose={() => {
          setSnackBar({ message: snackBar.message, open: false });
        }}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
      />
      {children}
    </SnackBarContext.Provider>
  );
};
